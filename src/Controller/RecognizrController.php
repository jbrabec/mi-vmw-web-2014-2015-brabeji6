<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Recognizr Controller
 *
 * @property App\Model\Table\RecognizrTable $Recognizr
 */
class RecognizrController extends AppController {

	public $components = array(
		'Cookie',
		'Session',
	);
	protected $_indexImage = false;
	protected $availableKeypointsEngines = array(
		'fast',
		'surf'
	);
	protected $keypointsEngine = 'fast';

	public function beforeFilter(\Cake\Event\Event $event) {
		parent::beforeFilter($event);

		if ($this->Cookie->check('keypoints_engine')) {
			$preferredEngine = $this->Cookie->read('keypoints_engine');
			if (in_array($preferredEngine, $this->availableKeypointsEngines)) {
				$this->keypointsEngine = $preferredEngine;
			}
		}
		$this->set('keypointsEngine', $this->keypointsEngine);
		$this->set('availableKeypointsEngines', $this->availableKeypointsEngines);
	}

	public function index() {
		
	}

	public function upload() {


		try {
			if ($this->request->is('post')) {
				$images = TableRegistry::get('Images');
				$data = $this->request->data;
				
				if (!isset($data['params'])) {
					$data['params'] = array();
				}
				$results = array();
				
				if (isset($data['images']['error']) && (bool) $data['images']['error']) {
					throw new \Exception('Invalid image.');
				}
				
				if (isset($data['image'])) {
					$data['images'] = array($data['image']);
				}
				foreach ($data['images'] as $key => $image) {
					if (isset($image['error']) && (bool) $image['error']) {
						throw new \Exception('Invalid image.');
					}

					$result = $images->processImage($image, 'index', $data['params'], $this->keypointsEngine);

					$query = $images->findById($result['indexedId']);
					$submittedImage = $query->first();
					$index = isset($data['index_image']) ? (bool) $data['index_image'] : false;
					if ($index) {
						$submittedImage->indexed = true;
						$images->save($submittedImage);
					}
					if (!isset($lastImage)) {
						$lastImage = $submittedImage;
					}


					$results[] = $result;
				}
				if (empty($results)) {
					$this->Flash->setError('Invalid image(s)');
					$this->redirect(['action' => 'index']);
				}
				if (count($results) == 1) {
					$this->redirect(['action' => 'library', $lastImage->hash]);
				}
				$this->redirect(['action' => 'library']);
			}
		} catch (\Exception $e) {
			$this->Flash->error($e->getMessage());
			$this->redirect($this->referer());
		}
	}

	public function setKeypointsEngine($engineId = 'fast') {
		if ($this->request->is('post')) {
			$this->Cookie->write('keypoints_engine', $engineId);
		}
		$this->redirect($this->referer());
	}

	public function library($imageHash = null) {
		$imagesTable = TableRegistry::get('Images');

		$commonConditions = ['indexed' => true, 'descriptors_' . $this->keypointsEngine . ' IS NOT' => null];

		if (!$imageHash) {
			$images = $imagesTable->find('all')->where($commonConditions)->all();
			$this->set('images', $images);
		} else {

			$imageExists = !empty($imagesTable->findByHash($imageHash)->where($commonConditions)->first());
			if (!$imageExists) {
				$this->redirect(['action' => 'library']);
			}

			try {
				if ($this->request->is('post')) {
					$data = $this->request->data;
					$result = $imagesTable->processImage($imageHash, 'both', $data['params'], $this->keypointsEngine);
				} else {
					$result = $imagesTable->processImage($imageHash, 'compare', array(), $this->keypointsEngine);
				}
			} catch (\Exception $e) {
				$this->Flash->set($e->getMessage());
				$this->redirect(['action' => 'library']);
			}

			$submittedImage = $imagesTable->findByHash($imageHash)->first();
			$this->set('submittedImage', $submittedImage);
			$this->set('image', $submittedImage);

			if (isset($result)) {
				if (isset($result['matches'])) {
					$matches = $result['matches'];
					asort($matches);
					$matches = array_reverse($matches, true);
					$query = $imagesTable->find()->where([
						array_merge($commonConditions, ['NOT' => [
								'hash' => $submittedImage->hash
					]])
					]);
					$order = implode(',', array_keys($matches));
					if (!empty($order)) {
						$query->order(['FIELD(id,' . $order . ')']);
					}
					$matchedImages = $query->all();
				}

				$this->set('matchedImages', $matchedImages);
				$this->set('matches', $matches);
				$this->set('timing', $result['timing']);
			}
		}
	}

	public function flush() {
		$images = TableRegistry::get('Images');
		$images->deleteAll([]);
		$this->redirect(['action' => 'index']);
	}

}
