<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\Core\Configure;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Images Model
 */
class ImagesTable extends Table {

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config) {
		$this->table('images');
		$this->displayField('id');
		$this->primaryKey('id');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator) {
		$validator
				->add('id', 'valid', ['rule' => 'numeric'])
				->allowEmpty('id', 'create')
				->validatePresence('filename', 'create')
				->notEmpty('filename');

		return $validator;
	}

	public function processImage($imageOrHash, $mode = 'both', $settings = array(), $keypointsEngine = 'fast') {
		$dds = ConnectionManager::get('default')->config();
		$connstr = '"' . $dds['host'] . (isset($dds['port']) ? ':' . $dds['port'] : '') . '|' . $dds['username'] . '|' . $dds['password'] . '|' . $dds['database'] . '"';
		// order matters
		$defaults = array(
			'match_acceptance_ratio' => 0.4,
			'fast_detector_threshold' => 40,
			'surf_detector_threshold' => 2800,
			'n_octaves' => 5,
			'n_octave_layers' => 4,
			//'hessian_threshold' => 30,
			'keypoints_engine' => $keypointsEngine,
			'mysql_connection_string' => $connstr
		);
		
		if ($settings) {
			$settings = array_merge($defaults, $settings);
		} else {
			$settings = $defaults;
		}
		
		
		if($keypointsEngine == 'fast') {
			unset($settings['surf_detector_threshold']);
		} else {
			unset($settings['fast_detector_threshold']);
		}
		
		///debug($settings);
		
		if (is_string($imageOrHash)) {
			$hash = $imageOrHash;
			$image = $this->find()->where([
						'hash' => $hash
					])->first();
			if ($image) {
				$target_file = $image->filename;
			} else {
				throw new \Exception('Image not found');
			}
		} else if (is_array($imageOrHash)) {
			$image = $imageOrHash;
			if (!empty($image) && !$image['error']) {
				$source_file = $image['tmp_name'];
				$original_ext = pathinfo($image['name'], PATHINFO_EXTENSION);

				$target_file = ROOT . Configure::read('Recognizr.data_images_path') . DS . md5_file($source_file) . '.' . $original_ext;
				move_uploaded_file($source_file, $target_file);
			} else {
				throw new \Exception('Invalid image');
			}
		}

		$cmd = Configure::read('Recognizr.utility_path') . ' ' .
				'--' . $mode . ' ' .
				$target_file . ' ' . implode(' ', $settings);
		$result = null;
		$status = 0;
		exec($cmd, $result, $status);
		if ($status !== 0) {
			debug($result);
			throw new \Exception('Invalid image.');
		}
		$result = json_decode(implode('', $result), true);
		

		return $result;
	}

}
