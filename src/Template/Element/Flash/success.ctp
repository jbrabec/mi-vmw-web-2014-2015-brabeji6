<div class="alert alert-success">
	<?= h($message) ?>
	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
</div>
