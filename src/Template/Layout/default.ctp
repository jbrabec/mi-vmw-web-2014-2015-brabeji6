<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
	<head>
		<?= $this->Html->charset() ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>
			recognizr
		</title>
		<?= $this->Html->css('site.css') ?>
		<style type="text/css">
			body {
				padding-top: 74px;
			}
		</style>

		<?= $this->Html->script('/bower_components/jquery/dist/jquery.min.js') ?>
		<?= $this->Html->script('/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>
		<?= $this->Html->script('http://imagesloaded.desandro.com/imagesloaded.pkgd.min.js') ?>
		<?= $this->Html->script('http://masonry.desandro.com/masonry.pkgd.min.js') ?>
		<?= $this->Html->script('main') ?>

		<?= $this->fetch('meta') ?>
		<?= $this->fetch('css') ?>
		<?= $this->fetch('script') ?>
	</head>
	<body>
		<div class="navbar-nav navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/">

						<span class="fa fa-image"></span>
						recognizr

					</a>
				</div> 
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li>
							<div class="btn navbar-btn menu-upload">
								<?php echo $this->Form->create('Image', array('type' => 'file', 'url' => ['action' => 'upload'])); ?>
								<?php echo $this->Form->hidden('index_image', array('value' => true)); ?>
								<?php echo $this->Form->file('images.', array('multiple' => true)); ?>
								<i class="fa fa-upload"></i> Upload
								<?= $this->Form->button('Upload', ['style' => 'display:none;']); ?>
								<?php echo $this->Form->end(); ?>
							</div>
						</li>
						<li><a href="/library"><i class="fa fa-book"></i> Library</a></li>
						<li><a href="/downloads/recognizr_samples.zip"><i class="fa fa-download"></i> Sample images</a></li>
					</ul>
					<div class="navbar-right">
						<p class="navbar-text">Keypoint extractor:</p>
						<ul class="nav navbar-nav keypoints-engine-switch">
							<?php
							foreach ($availableKeypointsEngines as $key => $value) {
								echo $this->Html->tag('li', $this->Form->postLink($value, ['action' => 'setKeypointsEngine', $value]), array('class' => ($value == $keypointsEngine ? 'active' : '')));
							}
							?>
						</ul>
					</div>
				</div><!--/.nav-collapse -->
			</div>
		</div>
		<div class="container">
			<?= $this->Flash->render() ?>
		</div>
		<?= $this->fetch('content') ?>
		<?php
		$credist = [
			'http://www.edwardrosten.com/work/fast.html' => 'FAST',
			'http://www.vision.ee.ethz.ch/~surf/index.html' => 'SURF',
			'http://opencv.org/' => 'OpenCV',
			'http://httpd.apache.org/' => 'Apache',
			'http://cakephp.org/' => 'CakePHP',
			'http://html5boilerplate.com/' => 'HTML5 ★ BOILERPLATE',
			'http://getbootstrap.com/' => 'Bootstrap 3',
			'http://bootstrapzero.com/bootstrap-template/blocks' => 'Blocks Bootstrap Template',
			'http://lesshat.madebysource.com/' => 'LESS Hat',
			'http://jquery.com/' => 'jQuery',
			'http://masonry.desandro.com/' => 'Masonry'
		];
		?>

		<footer class="footer">
			<div class="container">
				<ul class="credits-line">
					<?php foreach ($credist as $key => $value) { ?>
						<li>
							<a href="<?= $key ?>" target="_blank"><?= $value ?></a>
						</li>
					<?php } ?>
						
						<li>
							Jiří Brabec, l.p. 2014
						</li>
				</ul>
			</div>
		</footer>
	</div>
</body>
</html>
