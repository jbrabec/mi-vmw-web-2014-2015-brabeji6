<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<p>
				This app was created as semestral project for MI-VMW course of FIT CTU in Prague. Upload images or browse library.
			</p>
			<p>
				Tato aplikace vznikla jako semestrální práce pro předmět MI-VMW vyučovaný na FIT ČVUT v Praze. Vyberte obrázek, který má být porovnán s interní databází nebo si prohlédněte knihovnu.
			</p>
		</div>
	</div>
	<div class="dash-unit interactive clearfix">
		<div class="col-offset- col-xs-12">
			<dtitle>Image Upload</dtitle>
			<hr>

			<?php echo $this->Form->create('Image', array('type' => 'file', 'url' => ['action' => 'upload'])); ?>
			<div class="drag-n-drop upload-input" data-initial-title="drop image here">
				<?php echo $this->Form->file('images.', ['multiple' => true]); ?>
				<div class="text-center">
					<span class="fa fa-upload fa-huge"></span>
				</div>
				<h2>
					drop images here
				</h2>
				<div class="text-center">
					or<br /><br />
					<?php echo $this->Form->button('Choose files', ['class' => 'btn-choose-file btn btn-default btn btn-large']); ?>
				</div>
				<div class="checkbox">
					<label>
						<?php echo $this->Form->checkbox('index_image', array('hidden' => false, 'div' => false, 'label' => false, 'checked' => 'checked')); ?> Index images for search
					</label>
				</div>
				<div class="form-group upload-form">
					<?php //echo $this->Form->input('params.fast_detector_threshold', array('type' => 'number', 'default' => '40', 'div' => false, 'label' => 'FAST detector threshold', 'class' => 'form-control')); ?>
					<?php //echo $this->Form->input('params.match_acceptance_ratio', array('type' => 'number', 'default' => '0.5', 'step' => '0.01', 'div' => false, 'label' => 'Acceptance ratio', 'class' => 'form-control')); ?>
					<?php echo $this->Form->button('Upload', ['class' => 'btn btn-block btn-success btn btn-lg']); ?>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<?php echo $this->Html->link('Empty database', array('action' => 'flush'), array('confirm' => 'Are you absolutely sure you want to do that?')); ?>
</div>
