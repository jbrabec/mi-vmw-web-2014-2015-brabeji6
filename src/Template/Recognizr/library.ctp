<?php if (isset($images)) : ?>
	<div class="container">
		<div class="row library">
			<?php
			if (count($images)) :
				foreach ($images as $image) :
					$imgUri = explode('webroot', $image->filename);
					$imgUri = $imgUri[1];
					?>
					<div class="col-sm-12 col-lg-6">
						<div class="dash-unit interactive clearfix">
							<a href="<?php echo \Cake\Routing\Router::url(array('action' => 'library', $image->hash)); ?>">
								<div class="col-xs-12">
									<dtitle>
										<?php echo pathinfo($image->filename, PATHINFO_BASENAME); ?>
										<div class="date">
											<?php echo $image->created; ?>
										</div>
									</dtitle>
									<hr>
									<?php echo $this->Html->image($imgUri, array('class' => 'img-responsive')); ?>
								</div>
							</a>
						</div><!-- /dash-unit -->
					</div><!-- /span3 -->
					<?php
				endforeach;
			endif;
			?>
			<?php if (!count($images)) : ?>
				<div class="col-xs-12">
					<div class="empty-listing">
						no images to show
					</div>
				</div>
			<?php endif; ?>
		</div><!-- /row -->
	</div>
<?php endif; ?>




<?php if (isset($submittedImage)) : ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="dash-unit clearfix container">
					<div class="row">
						<div class="col-xs-12 col-lg-6">
							<dtitle>Query Image</dtitle>
							<hr>
							<?php
							if (isset($submittedImage)) :
								$imgUri = explode('webroot', $submittedImage->filename);
								$imgUri = $imgUri[1];
								echo $this->Html->image($imgUri, array('class' => 'img-responsive'));
							endif;
							?>
						</div>
						<div class="col-xs-12 col-lg-6">
							<dtitle><?php echo $submittedImage->{'n_keypoints_' . $keypointsEngine} ?> keypoints in <?php echo $timing['detection']; ?> &mu;s (descriptors extracted in <?php echo $timing['extraction']; ?> &mu;s)</dtitle>
							<hr>
							<div class="test-img-wrapper">
								<?php
								if (isset($submittedImage)) :
									$imgUri = explode('webroot', $submittedImage->filename);
									$imgUri = $imgUri[1];
									echo $this->Html->image('/data_images/' . $submittedImage->hash . '_keypoints_' . $keypointsEngine . '.png', array(
										'class' => 'img-responsive'
									));
								endif;
								?>
							</div>
						</div>
						<br />
						<div class="col-xs-12">

							<?php echo $this->Form->create('Image', array('type' => 'file', 'class' => 'form-horizontal')); ?>
							<fieldset>

								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<label class="col-xs-6 control-label">
												<?php echo strtoupper($keypointsEngine); ?> detector threshold
											</label>
											<div class="col-xs-6">
												<?php echo $this->Form->input('params.' . $keypointsEngine . '_detector_threshold', array('type' => 'number', 'default' => $submittedImage->{'keypoints_threshold_' . $keypointsEngine}, 'div' => false, 'label' => false, 'class' => 'form-control')); ?> 
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-6 control-label">
												Acceptance ratio
											</label>
											<div class="col-xs-6">
												<?php echo $this->Form->input('params.match_acceptance_ratio', array('type' => 'number', 'default' => '0.5', 'step' => '0.01', 'div' => false, 'label' => false, 'class' => 'form-control')); ?> 
											</div>
										</div>
									</div>
									<div class="col-xs-6">
										<?php echo $this->Form->button('Rematch', ['class' => 'btn btn-block btn-success btn btn-lg']); ?>
									</div>
								</div>

							</fieldset>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php if (isset($matchedImages)) : ?>
	<div class="container">
		<h2>Matching results (<?php echo $timing['matching']; ?> &mu;s)</h2>
	</div>
	<div class="container">
		<?php
		foreach ($matchedImages as $image) :
			$imgUri = explode('webroot', $image->filename);
			$imgUri = $imgUri[1];
			?>
			<div class="row">
				<div class="col-sm-3 col-lg-12">
					<div class="dash-unit">
						<div class="col-xs-12">
							<dtitle><?php echo $matches[$image->id]; ?> <?php echo ($matches[$image->id] != 1 ? 'matches' : 'match'); ?></dtitle>
							<hr>
							<?php
							echo $this->Html->image('/data_images/' . $submittedImage->hash . '_' . $image->hash . '_' . $keypointsEngine . '.png', array(
								'class' => 'img-responsive'
							));
							echo $this->Html->link('permalink', array('action' => 'library', $image->hash));
							?>
						</div>
					</div><!-- /dash-unit -->
				</div><!-- /span3 -->
			</div><!-- /row -->
			<?php
		endforeach;
		?>
		<?php if (!count($matchedImages)) : ?>
			<div class="row">
				<div class="col-sm-3 col-lg-12">
					<div class="empty-listing">
						no images to match
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif;