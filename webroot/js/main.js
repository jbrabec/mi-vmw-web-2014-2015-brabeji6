$(document).ready(function () {
	$('.btn-choose-file').each(function () {
		var $that = $(this);
		var dndArea = $that.closest('.drag-n-drop');
		var fileInput = dndArea.find('input[type="file"]');
		var heading = dndArea.find('h2');
		$that.on('click', function (e) {
			e.preventDefault();
			fileInput.click();
		});
		fileInput.on('change', function (e) {
			var fname = $(this).val().split(/[\\/]/).pop();
			if (fname) {
				heading.text(fname);
			} else {
				heading.text(dndArea.attr('data-initial-title'));
			}
			//dndArea.find('button').focus();
		});
	});

	$('.menu-upload').each(function () {
		var $that = $(this);
		var dndArea = $that.closest('.menu-upload');
		var fileInput = dndArea.find('input[type="file"]');
		fileInput.on('change', function (e) {
			fileInput.closest('form').submit();
		});
	});

	// or with jQuery
	var $container = $('.library');
// initialize Masonry after all images have loaded  
	$container.imagesLoaded(function () {
		$container.masonry();
	});

});